use strict;
use warnings;

#because we use strict flag, we need to write 'my' before each declaration

my $x = "This is a string";
print $x, "\n";

#In java we use 'type casting' for changing between types, put in perl no.

$x = 6;
print $x, "\n";
#arrays in perl is heterogeneous #each element in the array is a scalar variable

my @array = ("Ali", 6 , 7.8);
print @array, "\n";
print $array[1], "\n"; # we write $ sign because we want to reach a scalar value

my @array2 = ([1,2,3],[4,5,6],[7,8,9]);
print $array2[2][1], "\n";
print @{$array2[1]}, " \n";

# %grades =("Ali"=50,"Mehmet",75,"Ayşa",80) not a good idea for creating a hash

my %grades = ("Ali" => 50 ,"Mehmet" => 75 ,"Aysa" => 80);
print $grades{"Mehmet"}, "\n";

#'a' scalar takes the length of the array
my @array3 = ("one","two","three");
my $a = @array3;
print $a, " \n";
#'b' scalar have the value of the '0' index which is "one"
my ($b)= @array3;
print $b, "\n";
my ($b,$c,$d) = @array3;
# print '$b,$c,$d' will not work just normal string but "$b,$c,$d" works. This is called interpolation 
print "$b,$c,$d" , "\n";  

#To concatenate two variables , you have to use dot sign (plus sign in java and python to concatenate)
print $b.$c.$d , "\n";  
